/**Modules */
import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerContent from './src/components/drawer-content'
import AsyncStorage from '@react-native-community/async-storage';
import AuthContext from './src/context'
import * as SplashScreen from 'expo-splash-screen';
import * as Analytics from 'expo-firebase-analytics';

/**Screens */
import Authentication from './src/screens/authentication'
import Details from './src/screens/details'
import Home from './src/screens/home'
import Notifications from './src/screens/notifications'
import Payments from './src/screens/payments'
import Verification from './src/screens/verification'
import History from './src/screens/order-history'
import Loading from './src/screens/loading'
import Maps from './src/screens/maps'
import Paystack from './src/screens/pastack'

import { COLORS } from './src/utils/constants';



/**Navigations */
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const MainStack = () => {

  return(
      <Drawer.Navigator initialRouteName={"Home"} drawerContent={props => <DrawerContent {...props}/>} >
          <Drawer.Screen name="Home" component={HomeStack} />
          <Drawer.Screen name="Details" component={Details} />
          <Drawer.Screen name="Payments" component={Payments} />
          <Drawer.Screen name="History" component={History} />
          <Drawer.Screen name="Notifications" component={Notifications} />
      </Drawer.Navigator>
  )
}

const AuthStack = (props) => {
  return(
      <Stack.Navigator name="AuthStack" initialRouteName="Authentication">
        <Stack.Screen name="Authentication" component={Authentication} {...props} options={{ headerShown: false }} />
        <Stack.Screen name="Verification" component={Verification} {...props} options={{ headerShown: false }} />
      </Stack.Navigator>
  )
}

const HomeStack = (props) => {
  return(
      <Stack.Navigator name="Home" initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} {...props} options={{ headerShown: false }} />
        <Stack.Screen name="Details" component={Details} {...props} options={{ headerShown: false }} />
        <Stack.Screen name="Payments" component={Payments} {...props} options={{ headerShown: false }} />
        <Stack.Screen name="Directions" component={Maps} {...props} options={{ headerShown: false }} />
        <Stack.Screen name="Paystack" component={Paystack} {...props} options={{ headerShown: false }} />
      </Stack.Navigator>
  )
}



// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}




export default function App({ navigation }) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  React.useEffect(() => {

    async function showApp () {
      await SplashScreen.hideAsync()
    }

    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      //Keep splashscreen
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch (e) {
        console.warn(e);
      }

      let userToken;

      try {
        userToken = await AsyncStorage.getItem('authenticated');
      } catch (e) {
        // Restoring token failed
        userToken = null
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });
      showApp();
    };


    
    bootstrapAsync();
}, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async data => {
        console.log("yy",data)
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        if(data && data.auth){
          try {
            userToken = await AsyncStorage.setItem('authenticated', 'true');
            user_id = await AsyncStorage.setItem('userId', data.userId);
          } catch (e) {
            // Restoring token failed
            console.log("setting auth data failed", e)
          }
          
          dispatch({ type: 'SIGN_IN', token: 'true' });

        }else {
            dispatch({ type: 'SIGN_OUT'});
        }
        //dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signOut: async () => {

        try {
          await AsyncStorage.clear()
        } catch (e) {
          // Restoring token failed
        }

        dispatch({ type: 'SIGN_OUT' })
      },
      signUp: async data => {
        console.log(data)
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token

        try {
          userToken = await AsyncStorage.setItem('authenticated', 'false');
        } catch (e) {
          // Restoring token failed
        }

        dispatch({ type: 'SIGN_IN', token: 'false' });
      },
    }),
    []
  );
  

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer onStateChange={(prevState, currentState) => {
          const currentScreen = getActiveRouteName(currentState);
          const prevScreen = getActiveRouteName(prevState);
          if (prevScreen !== currentScreen) {
            // Update Firebase with the name of your screen
            Analytics.setCurrentScreen(currentScreen);
          }
        }}
      >
        <Stack.Navigator>
          {state.isLoading ? (
            // We haven't finished checking for the token yet
            <Stack.Screen name="Splash" component={Loading}  options={{ headerShown: false }}/>
          ) : state.userToken == null  ? (
            // No token found, user isn't signed in
            <Stack.Screen
              name="SignIn"
              component={AuthStack}
              options={{
                title: 'Sign In',
            // When logging out, a pop animation feels intuitive
                animationTypeForReplace: state.isSignout ? 'pop' : 'push',
                headerShown: false 
              }}
            />
          ) : (
            // User is signed in
            <Stack.Screen name="Home" component={MainStack}  options={{ headerShown: false }}/>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}