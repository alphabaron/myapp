import {CONFIG} from '../utils/constants'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'

export const REGISTER = async (data) => {

    let url = `${CONFIG.BASE_URL}/api/v1/users/createuser`;

    var response = {};
    
    const options = {
        method: 'POST',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }
    
    const request = await fetch(url, options);
    const results = await request.json();
    //console.log(results)

    if(results.code === 200){
        response.success = true
        response.data = results.response
        response.message = results.message
    }
    else {
        response.success = false
        response.data = null
        response.message = results.message
    } 

    return response
}



export const CREATE_ORDER = async (data) => {

    let url = `${CONFIG.BASE_URL}/api/v1/orders/createorder`;
    console.log(url)
    console.log(data)
    var response = {};
    
    const options = {
        method: 'POST',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }
    
    const request = await fetch(url, options);
    const results = await request.json();
    //console.log(results)

    if(results.code === 201){
        response.success = true
        response.data = results.response
        response.message = results.message
    }
    else {
        response.success = false
        response.data = null
        response.message = results.message
    } 

    return response
}



export const GET_ORDERS = async (userId) => {

    let url = `${CONFIG.BASE_URL}/api/v1/orders/getallordersbyid/${userId}`;
    console.log(url)

    var response = {};
    
    const options = {
        method: 'GET',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        }
    }
    
    const request = await fetch(url, options);
    const results = await request.json();
    //console.log(results)

    if(results.code === 200){
        response.success = true
        response.data = results.response
        response.message = results.message
    }
    else {
        response.success = false
        response.data = null
        response.message = results.message
    } 

    return response
}



export const LOGIN = async (data) => {

    let url = `${CONFIG.BASE_URL}/api/v1/users/login`;

    var response = {};
    
    const options = {
        method: 'POST',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }
    
    const request = await fetch(url, options);
    const results = await request.json();
    //console.log(results)

    if(results.code === 200){
        response.success = true
        response.data = results.response
        response.message = results.message
    }
    else {
        response.success = false
        response.data = null
        response.message = results.message
    } 

    return response
}



export const UPDATE_USER = async (data) => {

    let url = `${CONFIG.BASE_URL}/api/v1/users/updateuser/${data.userId}`;
    console.log(url)
    var response = {};
    
    const options = {
        method: 'PUT',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }

    try {
        const request = await fetch(url, options);
        const results = await request.json();
        //console.log(results)
        
        if(results.code === 201){
            response.success = true
            response.data = results.response
            response.message = results.message
        }
        else {
            response.success = false
            response.data = null
            response.message = results.message
        } 

        return response
    } catch (error) {
        console.log(error)
    }
    
    
}



export const GETBRANCHES = async (userLocation, country) => {
    //getallbrancheswithdistance/:country_code/:lat/:lng

    console.log(country)

    let url = `${CONFIG.BASE_URL}/api/v1/branches/getallbrancheswithdistance/${country}/${userLocation.lat}/${userLocation.lng}`;
    //console.log(url)
    var response = {};
    
    const options = {
        method: 'GET',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        }
    }
    try {
        const request = await fetch(url, options);
        const results = await request.json();
        //console.log(results)

        if(results.code === 200){
            response.success = true
            response.data = results.response
            response.message = results.message
        }
        else {
            response.success = false
            response.data = null
            response.message = results.message
        } 

        return response
    } catch (error) {
        console.log(error)
    }
    
}



export const VERIFY_USER = async (userId, otpCode) => {

    console.log(userId)
    console.log(otpCode)

    let url = `${CONFIG.BASE_URL}/api/v1/users/verifyuser/${userId}`;
    console.log(url)
    var response = {};

    var data = {
        otpCode: otpCode
    }
    const options = {
        method: 'PUT',
        cors: 'no-cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }
    
    const request = await fetch(url, options);
    const results = await request.json();
    //console.log(results)

    if(results.code === 200){
        response.success = true
        response.data = results.response
        response.message = results.message
    }
    else {
        response.success = false
        response.data = null
        response.message = results.message
    } 

    return response
}


export function distance(latLngA, latLngB) {
    //console.log(latLngA)
   // console.log(latLngB)
    var p = 0.017453292519943295;    // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((latLngB.lat - latLngA.lat) * p)/2 + c(latLngA.lat * p) * c(latLngB.lat * p) * (1 - c((latLngB.lng - latLngA.lng) * p))/2;
    //console.log(12742 * Math.asin(Math.sqrt(a)))
    var distance = parseInt(12742 * Math.asin(Math.sqrt(a)))
    return distance; // 2 * R; R = 6371 km
}

export function getNDistance(lat1, lon1, lat2, lon2) {
    // Converts numeric degrees to radians
    function toRad(Value) {
        return Value * Math.PI / 180;
    }

    var R = 6371; // km
    var dLat = toRad(lat2-lat1);
    var dLon = toRad(lon2-lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    return d;
}



export const STOREDATA = async (key, val) => {
    try {
      await AsyncStorage.setItem(key, val)
    } catch (e) {
      // saving error
      console.log(e)
    }
}

export const GETDATA = key => {
    try {
      var val = AsyncStorage.getItem(key)
      return val
    } catch (e) {
      // saving error
      console.log(e)
    }
}

export const CLEARDATA = async () => {
    try {
        await AsyncStorage.clear()
    } catch(e) {
        // clear error
    }
    
    console.log('Done.')
}

export const stateConditionString = state => {
    let navigateTo = '';
    if (state.isLoading) {
        navigateTo = 'LOAD_APP';
    }
    if (state.isSignedIn && state.userToken && state.isSignedUp) {
        navigateTo = 'LOAD_HOME';
    }
    if (!state.isSignedUp && state.noAccount) {
        navigateTo = 'LOAD_SIGNUP';
    }
    if (!state.isSignedIn && !state.noAccount) {
        navigateTo = 'LOAD_SIGNIN';
    }
    return navigateTo;
};


  /**
   * Better method for making http requests 
   * @param {string} url 
   * @param {string} method 
   * @param {Object} payload 
   */
  export async function httpRequest(url, method="get", payload=null){
    console.log("Request URL: ", url)
    console.log("Request Method: ", method)
    console.log("Payload: ", payload)

    if(method.toLowerCase() === "get"){

        const req = await axios.get(url, {validateStatus: false});
        const response = req.data
        //console.log(response)
        return response

    }else if(method.toLowerCase() === "post"){

        const req = await axios.post(url, payload, {validateStatus: false});
        const response = req.data
        console.log(response)
        return response

    }else if(method.toLowerCase() === "put"){

        const req = await axios.put(url, payload, {validateStatus: false});
        const response = req.data
        //console.log(response)
        return response

    }else if(method.toLowerCase() === "delete"){

        const req = await axios.delete(url, {validateStatus: false});
        const response = req.data
        //console.log(response)
        return response
        
    }else {
        return null
    }
}

