export const COLORS = {
    YELLOW: '#F5C46D',
    YELLOWVAR: '#F4CC82',
    WHITE: '#FFFFFF',
    GREY: '#00000029',
    DARKGREY: '#777474',
    NAVYBLUE: '#324755',
    SEABLUE: '#65AAD8',
    DARKNAVY: '#334856',
    ASH: '#D8D5D2',
    PEACH: '#F48282',
    BACkGROUND: '#EEEBEB',
    PETROL: '#F4CC82',
    DIESEL: '#ACACAC',
    VPOWER: '#1919192B',
    BLACK: '#000000',
    IDLE: '#F3F3F3'
    
}


export const CONFIG = {
    BASE_URL: "https://api.fuelsmartafrica.com" //"http://192.168.100.6:5600" //"https://api.fuelsmartafrica.com",
}

export const mapStyle = [
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f7f1df"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#d0e3b4"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.medical",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#fbd3da"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#bde6ab"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffe15f"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#efd151"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "black"
            }
        ]
    },
    {
        "featureType": "transit.station.airport",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#cfb2db"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#a2daf2"
            }
        ]
    }
]


export const PAYSTACK = {
    SECRET: "sk_test_2bad9d617f8de5bcedb7a5d588add490efdf8c0b",
    PUBLIC_KEY: "pk_test_db8e0cbeff36f664bab47d73de3274ba02b88441"
}