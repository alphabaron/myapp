import React from 'react'
import {View, StyleSheet, Dimensions, TouchableOpacity, Text, Share} from 'react-native'
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer'

import { COLORS } from '../utils/constants'
import IonIcon from 'react-native-vector-icons/Ionicons'
import { CLEARDATA } from '../utils/functions'
import { NavigationActions } from '@react-navigation/compat'
import AuthContext from '../context'

export default function DrawerContent(props){

    const { signOut } = React.useContext(AuthContext);

    const onShare = async () => {
        try {
          const result = await Share.share({
            message: 'Fuel App',
          });
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
    };
    
      
    return(
        <View style={{flex: 1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}> 
                    {/*<View style={styles.userInfoSection}>
                        <View style={{flexDirection: 'row', paddingVertical: 20}}>
                            <Avatar.Image
                                source={{uri:'https://picsum.photos/200'}}   
                                size= {60}
                            />
                            <View style={{paddingHorizontal:20, flexDirection: 'column', justifyContent: 'center'}}>
                                <Title style={styles.title}>Abert</Title>
                                <Caption style={styles.caption}>Dev</Caption>
                            </View>
                        </View>
                    </View>  */}                  
                </View>
                <View style={styles.drawerSection}>
                    <TouchableOpacity onPress={()=> props.navigation.navigate("Home")}>
                        <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 20, borderBottomColor: COLORS.YELLOW, borderBottomWidth: 1}}>
                            <IonIcon name="ios-home" size={32} color={COLORS.YELLOW} />
                            <View style={{width: '100%', justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: 20}}>
                                <Text style={{color: COLORS.BLACK, fontWeight: 'bold'}}>Home</Text>
                            </View>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={()=> props.navigation.navigate("History")}>
                        <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 20, borderBottomColor: COLORS.YELLOW, borderBottomWidth: 1}}>
                            <IonIcon name="ios-time" size={32} color={COLORS.YELLOW} />
                            <View style={{width: '100%', justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: 20}}>
                                <Text style={{color: COLORS.BLACK, fontWeight: 'bold'}}>History</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    {/*<TouchableOpacity onPress={()=> onShare()}>
                        <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 20, borderBottomColor: COLORS.YELLOW, borderBottomWidth: 1}}>
                            <IonIcon name="ios-person-add" size={32} color={COLORS.YELLOW}  />
                            <View style={{width: '100%', justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: 20}}>
                                <Text style={{color: COLORS.BLACK, fontWeight: 'bold'}}>Invite a friend</Text>
                            </View>
                        </View>
            </TouchableOpacity>*/}


                    <TouchableOpacity onPress={()=> {signOut()}} >
                        <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 20, borderBottomColor: COLORS.YELLOW,   borderBottomWidth: 1}}>
                            <IonIcon name="ios-exit" size={32}  color={COLORS.YELLOW} />
                            <View style={{justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: 20}}>
                                <Text style={{color: COLORS.BLACK, fontWeight: 'bold'}}>Sign out</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    
                </View>
            </DrawerContentScrollView>
        </View>
    )   
}


const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
        paddingBottom: width * 0.3,
        borderBottomColor: COLORS.YELLOW,
        borderBottomWidth: 1
    },
    userInfoSection: {
        paddingHorizontal: 10,
        paddingVertical: 30,
        height: width * 0.4,
        backgroundColor: COLORS.WHITE,
        borderBottomColor: COLORS.YELLOW,
        borderBottomWidth: 2
    },
    title:{
        fontSize: 18,
        paddingVertical: 2,
        fontWeight: 'bold'
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    }, 
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3
    },
    drawerSection: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
    },
    bottomDrawerSection: {
        
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16
    }
})