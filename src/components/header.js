import React from 'react';
import {View, Text, StyleSheet} from 'react-native'
import IonIcons from 'react-native-vector-icons/Ionicons'

export default function Header(props){

    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <View style={styles.icon}>
                    <IonIcons name="ios-home" size={32} />
                </View>
                <View style={styles.screenName}>
                    <Text style={styles.title}>Name</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    head: {

    },
    icon: {
        width: '30%'
    },
    screenName: {
        width: '70%',
    },
    title: {
        fontSize: 50,
        fontWeight: 'bold'
    }
})