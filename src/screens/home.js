import React, { useEffect, useState } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, FlatList, TextInput, Dimensions, RefreshControl, ActivityIndicator, ScrollView, Platform} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import { Avatar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { GETBRANCHES } from '../utils/functions';
import * as Location from 'expo-location';
import AuthContext from '../context';
import {AdMobBanner} from 'expo-ads-admob';
import {
    Placeholder,
    PlaceholderMedia,
    PlaceholderLine,
    Fade
  } from "rn-placeholder";

export default function Home(props){

    const { signIn, signOut } = React.useContext(AuthContext);

    const [isMounted, setMounted] = useState(true)
    const [location, setLocation] = useState(null);
    const [_temp, setTemp] = useState([])
    const [errorMsg, setErrorMsg] = useState(null);
    const [latLng, setLatLng] = useState(null)
    const [searchTerm, setSearchTerm] = useState("")
    const [isReady, setReady] = useState(false)
    const [country, setCountry] = useState(null)

    const navigation = useNavigation();

    function searchItems(searchTerm){
        var temp = _temp;

        var filtered = temp.filter(item => {
            console.log(item)
            return item.name.toLowerCase().includes(searchTerm.toLowerCase()) || item.parent_brand.name.toLowerCase().includes(searchTerm.toLowerCase()) || item.address.toLowerCase().includes(searchTerm.toLowerCase())
        })
        setBranches(filtered)
        
    }

    function wait(timeout) {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }
    const [refreshing, setRefreshing] = useState(false);
    const [branches, setBranches] = useState(null)

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getLocation()
        wait(2000).then(() => setRefreshing(false));
    }, [refreshing]);



    const ItemComponent = ({item}) => (
        <><View style={{flexDirection: 'row', paddingHorizontal: 10, paddingTop: 5, justifyContent: 'space-between'}}>
            
            <TouchableOpacity onPress={()=>{props.navigation.push('Directions', {item: JSON.stringify({...item, fuelName: 'Diesel', fuelPrice: item.parent_brand.diesel_price, userLocation: latLng})})}}>
                <Text style={{color: COLORS.DARKGREY}}>{item.name}</Text>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={()=>{props.navigation.push('Directions', {item: JSON.stringify({...item, fuelName: 'Diesel', fuelPrice: item.parent_brand.diesel_price, userLocation: latLng})})}}>
                <Text style={{color: COLORS.DARKGREY}}>{item.distance.toString()} km away</Text>
            </TouchableOpacity>
            
        </View>
        <View style={{marginBottom: 20, marginTop: 2, marginHorizontal: 5, backgroundColor: COLORS.WHITE, borderRadius: 10,}}>
            <View style={styles.item}>
                <View style={styles.avatar}>
                    <TouchableOpacity onPress={()=> navigation.push('Details', {
                        item: JSON.stringify(item),
                    })}>
                        <Avatar.Image size={50} source={{uri: item.parent_brand.logo}} backgroundColor="#fff" style={{backgroundColor: '#fff'}} />
                    </TouchableOpacity>
                </View>
                
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {item.parent_brand.products && item.parent_brand.products.length > 0 && item.parent_brand.products.map((i, index) => (
                        <View key={index}  style={{...styles.itemShadow, ...styles.petrol, marginBottom: 5}}>
                            <TouchableOpacity onPress={()=>{ navigation.push('Payments', {item: JSON.stringify({...item, fuelName: i.name, fuelPrice: i.price})})}}>
                                <Text style={styles.fuelname}>{i.name}</Text>
                                <Text style={styles.fuelprice}>{item.parent_brand.currency_symbol} {i.price}</Text>
                            </TouchableOpacity>
                        </View>
                ))}
                {item.parent_brand.products && item.parent_brand.products.length > 0 && Array(5 - item.parent_brand.products.length).fill().map((x, index) => (
                        <View key={index} style={{...styles.itemShadow, ...styles.diesel, marginBottom: 5}}>
                              
                        </View>
                ))}
                </ScrollView>
            </View>
        </View></>
    )

    async function getAllBranches(latLng, country){
        console.log("Getting branches in ", country)
        try {
            const c = await GETBRANCHES(latLng, country);
            if(c.success){
                console.log("Successfully retreieved branches")
                //console.log("Branches: ", c.data)
                setBranches(c.data)
                setTemp(c.data)
            }
            else {
                console.log("Error occured getting branches")
                //console.log(c.message)
            }
        } catch (error) {
            console.log("Errors attempting to get branches: ", error)
            
        }
       
    }

    
    async function getLocation () {
        try {
                let { status } = await Location.requestPermissionsAsync();
                if (status !== 'granted') {
                    setErrorMsg('Permission to access location was denied');
                    console.log('Permission to access location was denied')
                } else {
                    let deviceLocation = await Location.getCurrentPositionAsync({});
                    setLocation(deviceLocation);
                    //console.log("Device Location: ", deviceLocation)
                    setLatLng({lat: deviceLocation.coords.latitude, lng: deviceLocation.coords.longitude})
                    
                    var c;
                    if(country == null){
                        let f = {latitude: deviceLocation.coords.latitude, longitude: deviceLocation.coords.longitude}
                        let reverseGeo = await Location.reverseGeocodeAsync(f)
                        //console.log(reverseGeo)
                        setCountry(reverseGeo[0].country)
                        //console.log("Country: ", reverseGeo[0].country)
                        c = reverseGeo[0].country
                    }

                    console.log("reguesting...")
                    getAllBranches({lat: deviceLocation.coords.latitude, lng: deviceLocation.coords.longitude}, country ? country : c);
                }
            
            
        } catch (error) {
            console.log("Location request error: ", error)
            setErrorMsg("Error attempting to get location information")
        }
    }
    
    
    function bannerError(){
        console.log("Ads failed")
    }
    
    
    useEffect(()=>{
        console.log("mounted")
        if(isMounted){
            getLocation();
        }
        const interval = setInterval(getLocation, 60000);
    
        
        return () => {
            clearInterval(interval);
            setMounted(false)

            console.log("unmounted")
        }
    },[isMounted, location])


    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.toggleDrawer()}><IonIcons color={COLORS.BLACK} size={45} name="ios-menu" /></TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>Home</Text>
                    </View>
                </View> 
                <View style={styles.topButtons}>
                    <View style={styles.topItem}>
                        <View style={{flexDirection: 'row', paddingVertical: 5, backgroundColor: COLORS.WHITE, borderRadius: 10, width: width * 0.9, justifyContent: 'center' }}>
                            <TextInput style={{width: searchTerm.length > 0 ? '90%' : '100%', fontSize: 20, paddingHorizontal: 10}} value={searchTerm} onChangeText={(text)=>{searchItems(text); setSearchTerm(text)}} placeholder="Search..." />

                            {searchTerm.length > 0 &&<TouchableOpacity style={{justifyContent: 'center'}} onPress={()=>{setSearchTerm(""); setBranches(_temp)}}>
                                <IonIcons name="ios-close" color="grey" size={20} style={{paddingHorizontal: 10}} />
                            </TouchableOpacity>}
                        </View>
                    </View>  
                </View>
            </View>

            {!branches ? branches.length > 0 ? 
            
                <View style={styles.content}>
                    <FlatList
                        data={branches.sort(function(a, b) {
                            return a.distance - b.distance;
                        })}
                        renderItem={ItemComponent}
                        keyExtractor={(item, index)=> index.toString()}
                        showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                        }
                    />
                    <AdMobBanner
                        bannerSize="banner"
                        adUnitID={Platform.OS === 'ios' ? "ca-app-pub-7107854782238747/9873876660" : "ca-app-pub-7107854782238747/4947168831"} // Test ID, Replace with your-admob-unit-id
                        servePersonalizedAds // true or false
                        onDidFailToReceiveAdWithError={bannerError} />
                </View> : 
            
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <Text> No fuel stations availble</Text>
                </View> :  
                
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large" color={COLORS.PEACH} />
                </View>}
        </View>
    )
    
    
}

const width = Dimensions.get("screen").width

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        flexDirection: "row",
        paddingTop: 30,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    section: {
        width: width * 0.5,
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 50,
        backgroundColor: '#f2f2f2',
        marginTop: 20
    },
    title: {
        fontSize: width * 0.07,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: COLORS.WHITE
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    loaderitem: {
        flexDirection: 'row',
        paddingVertical: 8,
        paddingHorizontal: 2,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.YELLOWVAR,
        padding: 10,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: width * 0.03,
        width: width * 0.19,
        height: width * 0.19
    },
    loader: {
       // padding: 10,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: width * 0.03,
        width: width * 0.19,
        height: width * 0.19
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 10,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: width * 0.03,
        width: width * 0.19,
        height: width * 0.19
    },
    vpower: {
        backgroundColor: COLORS.ASH,
        padding: 10,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: width * 0.028,
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'flex-start'
    },
    fuelprice:{
        fontSize: width * 0.028,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    itemShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    }

})