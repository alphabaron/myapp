import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, Image, ScrollView} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'

export default function Details(props){

    //const { name } = props.route.params;
    const [itemObj, setItem] = useState(null)
    

    useEffect(()=>{
        let isSubscribed = true

        if(isSubscribed){
            const item = JSON.parse(props.route.params.item);
            setItem(item)
            console.log()
        }

        return () => {
            isSubscribed = false
        }
       
    }, [])

    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.goBack()}><IonIcons color={COLORS.BLACK} size={45} name="ios-arrow-round-back" /></TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center', justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>{itemObj && itemObj.name}</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tabbar}>
                <View style={{alignSelf: 'center'}}>
                    <View style={styles.profileImageSection}>
                        <Image source={itemObj ? {uri: itemObj.parent_brand.logo } : require('../assets/load.png')} style={styles.profileImage}/>
                    </View>
                </View>
                <View style={styles.cardView}>
                    <Text style={styles.itemText}>Address</Text>
                    <View style={{...styles.itemCard, ...styles.itemShadow}}>
                        <Text style={styles.cardText}>{itemObj && itemObj.address}</Text>
                    </View>

                    <Text style={styles.itemText}>Contact</Text>
                    <View style={{...styles.itemCard, ...styles.itemShadow}}>
                        <Text style={styles.cardText}>{itemObj && itemObj.phone_numbers.join(",")}</Text>
                    </View>

                    <Text style={styles.itemText}>Facilities</Text>
                    <View style={{...styles.itemCard, ...styles.itemShadow}}>
                        <Text style={styles.cardText}>{itemObj && itemObj.facilities.join(",")}</Text>
                    </View>
                </View>
            </View>
            </ScrollView>
        </View>
    )
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        flexDirection: "row",
        paddingTop: 30,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: width * 0.07,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 20,
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    profileImageSection: {
        width: 200,
        height: 200,
        borderRadius:100,
        marginBottom: 10
    },
    profileImage: {
        width: 200, 
        height: 200,
    },
    cardView: {
        paddingHorizontal: 5,
        paddingVertical: 10
    },
    itemShadow: {
        //ios
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.2,
        shadowRadius: 4,

        //android
        elevation: 5,
    },
    itemCard: {
        //flexDirection: "row", 
        //justifyContent: 'space-between', 
        //alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 20,
        backgroundColor: "#fbfbfb",
        borderRadius: 5,
        marginBottom: 20
    },
    itemText: {
        fontSize: 15,
        fontWeight: 'bold',
        paddingVertical: 5
    },
    cardText: {
        fontSize: 15,
    }
})