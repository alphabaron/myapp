import React from 'react';
import {View, Text, StyleSheet} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'


const PricesComponent = (props) => (
    <View style={styles.prices}>
        <Text style={styles.name}>Petrol</Text>
        <View style={styles.fuelpriceContainer}><Text style={styles.fuelprice}>GH₵ 5.22</Text></View>
        <View style={styles.icon}>
            <View style={{flexDirection: "row"}}>
                <IonIcons name="ios-arrow-round-up" size={32} color={'#FF001A'}/>
                <IonIcons name="ios-arrow-round-down" size={32} color={'#D8D5D2'}/>
            </View>
        </View>
        <Text style={styles.date}>17/04/20</Text>
    </View>
)

export default function Notifications(props){

    return (
        <View style={styles.container}>
            <View style={styles.section}>
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    section: {

    },
    title: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    prices: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignContent: 'center'
    },
    name: {
        color: COLORS.ASH,
        fontWeight: 'bold',
        paddingVertical: 10,
        marginHorizontal: 20,
        fontSize: 18
    },
    fuelpriceContainer: {
        backgroundColor: COLORS.YELLOWVAR,
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 20,
        marginHorizontal: 20,
       
    },
    fuelprice: {
        justifyContent: 'center',
        alignContent: 'center',
        fontSize: 18
    },
    icon: {
        justifyContent: 'center',
        alignContent: 'center'
    },  
    date: {
        color: COLORS.ASH,
        fontWeight: 'bold',
        paddingVertical: 10,
        marginHorizontal: 20,
        fontSize: 18
    }

})