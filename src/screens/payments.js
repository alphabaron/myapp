import React, {useEffect, useState, useRef, useContext} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView, TextInput, Alert, Image, ActivityIndicator} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import {CREATE_ORDER, httpRequest} from '../utils/functions'
import {Picker} from '@react-native-community/picker';
import IntlPhoneInput from 'react-native-intl-phone-input';
import AuthContext from '../context';

export default function Payments(props){
    const paystackWebViewRef = useRef();
    const ref = useRef(null);
    //const { name } = props.route.params;
    const [itemObj, setItem] = useState(null)
    const [userId, setUserId] = useState("")
    const [wallet, setWallet] = useState("")
    const [vehicle, setVehicle] = useState("")
    const [amount, setAmount] = useState("")
    const [isDisabled, setDisabled] = useState(false)
    const [btnText, setBtnText] = useState("Continue")
    const [info, setInfo] = useState("Make Payment")
    const [plat, setPlatform] = useState("momo")
    const [firstname, setFirstname] = useState("")
    const [lastname, setLastname] = useState("")
    const [email, setEmail] = useState("")
    const [mobile, setMobile] = useState("")
    const [showPayment, setShowPayment] = useState(false);
    const [network, setNetwork] = useState("MTN")
    const [isNetworkSet, setNetworkAvailable] = useState(false)
    const [ready, setReady] = useState(false)

    const { signOut } = useContext(AuthContext);

    function confirm(){
        setBtnText("Creating order...")
        if(vehicle && amount){
            Alert.alert(
                "Confirmation",
                `Confirm Payment of ${itemObj && itemObj.parent_brand.currency_symbol} ${amount} for ${itemObj && itemObj.fuelName} at ${itemObj.name} `,
                [{
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                }, { text: "YES", onPress: () => MakePayment() }
                ],
                { cancelable: false }
            );
        } else {
            Alert.alert("Validation error", "Input fields required");
        }
    }
    
    async function MakePayment(){

        //create order
        let data = {
            product: itemObj && itemObj.fuelName,
            wallet: mobile,
            order_for: vehicle,
            amount: parseFloat(amount).toFixed(2),
            user: userId,
            branch:  itemObj._id,
            mobile: mobile,
            amountToPay: parseFloat(amount),
            network: network,
            currency: itemObj && itemObj.parent_brand && itemObj.parent_brand.currency_symbol
        }

        try {
            var c = await CREATE_ORDER(data)
            if(c.success){
               // setBtnText("Continue")
                console.log(c.data)
                var paymentPayload = c.data
                
                var URL = itemObj && itemObj.parent_brand && itemObj.parent_brand.country === 'ghana' ? 'https://api.fuelsmartafrica.com/api/v1/allpayments/ghana/requesttopay' : 'https://api.fuelsmartafrica.com/api/v1/allpayments/zambia/requesttopay'

                var p = await httpRequest(URL, "post", paymentPayload)
                if(p.code === 200){
                    Alert.alert("Success", c.message + ". Payment will be initiated on your mobile money number to complete the order");
                    
                    props.navigation.push("Home")

                }else {
                    Alert.alert("Failed", c.message)
                }
            }
            else{
                setBtnText("Continue")
                console.log(c.message)
                setInfo(c.message)
                Alert.alert("Error", c.message)
            }

        } catch (error) {
            setBtnText("Continue")
            console.log(error)
            Alert.alert("Error", error, "An error occured. Please try again")
        }
    }

    useEffect(()=>{
        let isSubscribed = true

        if(isSubscribed){
            const item = JSON.parse(props.route.params.item);
            setItem(item)
            console.log()
        }

        return () => {
            isSubscribed = false
        }
    }, [])


    async function getUserId(){
        try {
            var value = await AsyncStorage.getItem("userId")
            if(value !== null){
                setUserId(value)

                //getuser data
                var url = `https://api.fuelsmartafrica.com/api/v1/users/getuserbyid/${value}`
                var c = await httpRequest(url)
                if(c.code === 200){
                    console.log("User data: ", c.response)

                    var p = c.response[0].phone
                    p = p.replace("+", "")
                    setMobile(p)
                    setWallet(p)
                    
                    if(c.response[0].network){
                        setNetwork(c.response[0].network)
                        setNetworkAvailable(true)
                    }
                    else {
                        setNetworkAvailable(false)
                    }
                }else {
                    console.log(c.message)
                    setNetworkAvailable(false)
                }
            } else {
                console.log("User data request failed")
                signOut()
            }
        } catch (error) {
            console.log(error)
            signOut()
        }

        setReady(true)
    }

    async function _handleNetworkUpdate(value){
        setNetwork(value)
        //update user
        var url = `https://api.fuelsmartafrica.com/api/v1/users/updateuser/${userId}`
        var payload = {
            network: value
        }
        var c = await httpRequest(url, "put", payload)
        if(c.code === 201){
            console.log(c.message)
        } else {
            console.log(c.message)
        }
    }

    const onChangeText = ({dialCode, unmaskedPhoneNumber, phoneNumber, isVerified}) => {
        //console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
        var p = dialCode + unmaskedPhoneNumber
        var p = p.replace("+", "")
        setWallet(p);
        setMobile(p);
        console.log(p)
    };

    useEffect(()=>{
        getUserId()
        const item = JSON.parse(props.route.params.item);
        setItem(item)
        //console.log(item)
    },[])

    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.goBack()}><IonIcons color={COLORS.BLACK} size={45} name="ios-arrow-round-back" /></TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>Order</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tabbar}>
                {ready ? <ScrollView showsVerticalScrollIndicator={false} style={{paddingHorizontal: 20}}>

                    <View style={styles.inputContainer}>
                        <Text style={styles.inputTitle}>Vehicle Registration Number</Text>
                        <TextInput placeholder="Enter the registration number of the vehicle" value={vehicle} onChangeText={text => setVehicle(text)} style={{...styles.input, borderBottomColor: COLORS.BLACK}} />
                    </View>

                    <View style={styles.inputContainer}>
                        <Text style={styles.inputTitle}>Amount</Text>
                        <TextInput placeholder="Amount of fuel" value={amount} onChangeText={text => setAmount(text)} style={{...styles.input, borderBottomColor: COLORS.BLACK}} keyboardType={"number-pad"} />
                    </View>

                    {!isNetworkSet && <View style={styles.inputContainer}>
                        <Text style={styles.inputTitle}>Add your mobile phone network</Text>
                        <Picker
                            selectedValue={network}
                            style={{...styles.input, borderBottomColor: COLORS.BLACK}}
                            onValueChange={(itemValue, itemIndex) => _handleNetworkUpdate(itemValue)}
                        >
                            <Picker.Item label="MTN" value="MTN" />
                            <Picker.Item label="AIRTEL TIGO" value="TIGO" />
                            <Picker.Item label="VODAFONE" value="VODAFONE" />
                        </Picker>
                    </View>}

                   
                </ScrollView> : 
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large" color={COLORS.PEACH} />
                </View>}
            </View>

            <TouchableOpacity disabled={isDisabled} style={styles.saveBtn} onPress={()=> confirm() }>
                <Text style={{fontWeight: 'bold'}}>Pay</Text>
            </TouchableOpacity>
        </View>
    )
    
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        flexDirection: "row",
        paddingTop: 30,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 20,
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
    },
    inputContainer: {
        paddingVertical: 30,       
    },
    platform: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        //paddingVertical: 10,   
    },
    input:{
        marginTop: 10,
        borderBottomWidth: 1,
        color: COLORS.BLACK
    },
    inputTitle:{
        fontWeight: 'bold'
    },
    saveBtn: {
        width: '100%',
        height: 60,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    },
    btnText: {
        color: 'black',
        fontSize: 15,
    },
})