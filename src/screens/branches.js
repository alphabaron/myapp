import React from 'react';
import {View, Text, StyleSheet} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'


export default function Branches(props){

    const PricesComponent = (props) => (
        <View style={styles.branch}>
            <Text style={styles.name}>Name: </Text>
            <Text style={styles.address}>Address: </Text>
            <Text style={styles.gpscode}>GPS CODE: </Text>
            <Text style={styles.phone}>Phone: </Text>
        </View>
    )

    return (
        <View style={styles.container}>
            <View style={styles.section}>
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
                <PricesComponent />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold',
        color: COLORS.DARKGREY
    },
    address: {
        fontSize: 16,
        color: COLORS.DARKNAVY
    },
    gpscode: {
        fontSize: 16,
        color: COLORS.DARKNAVY
    },
    phone: {
        fontSize: 16,
        color: COLORS.DARKNAVY
    },
   
    branch: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignContent: 'center',
        borderBottomWidth: .5,
        borderBottomColor: COLORS.YELLOW
    },
   

})