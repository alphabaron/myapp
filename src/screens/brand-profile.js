import React, { useEffect, useState } from 'react';
import {View, Text, StyleSheet, ScrollView, Dimensions} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import {Avatar, Title, Caption} from 'react-native-paper'

const FacilityComponent = (props) => (
    <View style={styles.facilities}>
        <Text style={styles.name}>Petrol</Text>
        <Text style={styles.name}>Petrol</Text>
        <Text style={styles.name}>Petrol</Text>
    </View>
)

export default function BrandProfile(props){

    const [itemObj, setItem] = useState(null)
    const [isMounted, setMounted] = useState(true)

    useEffect(()=>{
        

        if(isMounted){
            const item = JSON.parse(props.props.route.params.item);
            setItem(item)
            console.log(item)
        }

        return function(){
            setItem(null)
            setMounted(false)
        }
    }, [])


    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.section}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection: 'row', paddingHorizontal:10, justifyContent: 'flex-start'}}>
                            <Avatar.Image
                                source={{uri: itemObj && itemObj.parent_brand.logo}}   
                                backgroundColor="#fff"
                                size= {35}
                            />
                            <View style={{paddingHorizontal:10, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
                                <Title style={styles.nametitle}>{itemObj && itemObj.parent_brand.name}</Title>
                            </View>
                        </View>
                    </View>    
                </View>
                <View style={styles.section}>
                    <View style={{paddingHorizontal: 10}}>
                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.itemTitle}>Working Hours </Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.parent_brand.working_hours}</Text>
                        </View>

                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.itemTitle}>Facilities  </Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.facilities.join(", ")}</Text>
                        </View>

                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.itemTitle}>Contacts  </Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.phone_numbers.join(", ")}</Text>
                        </View>
                       
                    </View>    
                </View>
                {/*<View style={styles.section}>
                    <Text style={styles.title}>Products</Text>
                    <View style={{paddingHorizontal: 10}}>
                        <View style={{flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between'}}>
                            <Text style={styles.itemTitle}>Petrol - {itemObj && itemObj.parent_brand.currency_symbol} {itemObj && itemObj.parent_brand.petrol_price}/L</Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.parent_brand.petrol_available ? 'Available' : 'Unavailble'}</Text>
                        </View>

                        <View style={{flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between'}}>
                        <Text style={styles.itemTitle}>Diesel - {itemObj && itemObj.parent_brand.currency_symbol} {itemObj && itemObj.parent_brand.petrol_price}/L</Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.parent_brand.diesel_available ? 'Available' : 'Unavailble'}</Text>
                        </View>

                        <View style={{flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between'}}>
                        <Text style={styles.itemTitle}>V-Power - {itemObj && itemObj.parent_brand.currency_symbol} {itemObj && itemObj.parent_brand.petrol_price}/L</Text>
                            <Text style={styles.itemText}>{itemObj &&  itemObj.parent_brand.vpower_available ? 'Available' : 'Unavailble'}</Text>
                        </View>
                    </View>
                </View>*/}
            </ScrollView>
        </View>
    )
}


const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    section: {
        paddingVertical: 10
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    nametitle: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    facilities: {
        justifyContent: 'center',
        alignContent: 'center'
    },
    name: {
        color: COLORS.ASH,
        fontWeight: 'bold',
        paddingVertical: 10,
        marginHorizontal: 20,
        fontSize: 18
    },
    fuelpriceContainer: {
        backgroundColor: COLORS.YELLOWVAR,
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 20,
        marginHorizontal: 20,
       
    },
    fuelprice: {
        justifyContent: 'center',
        alignContent: 'center',
        fontSize: 18
    },
    icon: {
        justifyContent: 'center',
        alignContent: 'center'
    },  
    date: {
        color: COLORS.ASH,
        fontWeight: 'bold',
        paddingVertical: 10,
        marginHorizontal: 20,
        fontSize: 18
    },
    userInfoSection: {
        //height: width * 0.4,
    },
    itemTitle: {
        marginRight: 10,
        fontWeight: 'bold',
        color: COLORS.BLACK,
    },
    itemText: {
      
    },
})