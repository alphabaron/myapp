import React from 'react';
import MapView, {Marker} from 'react-native-maps';
import { StyleSheet, Text, View, Dimensions, ActivityIndicator, TouchableOpacity } from 'react-native';
import MapViewDirections from 'react-native-maps-directions'
import { COLORS, mapStyle } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import * as Location from 'expo-location';

export default class Maps extends React.Component {

    constructor(props){
      super();
      this.state = {
        item: null,
        destination: null,
        origin: null,
        ready: false,
        errorMsg: "",
        latLng: null,
        isMounted: true
      }

      this.getLocation = this.getLocation.bind(this)
    }

    intervalId = 0

    async getLocation () {
        try {
          let { status } = await Location.requestPermissionsAsync();
          if (status !== 'granted') {
              this.setState({
                errorMsg: 'Permission to access location was denied'
              }, ()=>{
                console.log(this.state.errorMsg)
              })
          } else {
              let deviceLocation = await Location.getCurrentPositionAsync({});
              console.log(deviceLocation)
              this.setState({
                latLng: {latitude: deviceLocation.coords.latitude, longitude: deviceLocation.coords.longitude},
                origin: {latitude: deviceLocation.coords.latitude, longitude: deviceLocation.coords.longitude},
              })
          }
        } catch (error) {
            console.log(error)
        }
    }


  componentDidMount(){
    const data = JSON.parse(this.props.route.params.item);
    console.log("data", data)

    this.state.isMounted && this.getLocation().then(res => {
     
      this.setState({
        item: data,
        destination: data.geolocation,
        ready: true
      }, ()=> {
        console.log(this.state.destination)
        console.log(this.state.origin)
      })
    })

    this.intervalId = setInterval(this.getLocation, 60000);
    
  }
  
  componentWillUnmount(){
    this.setState({
      isMounted: false
    })
    clearInterval(this.intervalId);
  }

  
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topSection}>
            <View style={styles.header}>
                <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()}><IonIcons color={COLORS.BLACK} size={45} name="ios-arrow-round-back" /></TouchableOpacity>
                </View>
                <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                    <Text style={styles.title}>Directions</Text>
                </View>
            </View>
        </View>
        {this.state.ready ?
        
            <MapView style={styles.mapStyle} 
              region={{
                latitude: this.state.origin.latitude,
                longitude: this.state.origin.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }} 
            >
              <Marker coordinate={{latitude: this.state.origin.latitude,
              longitude: this.state.origin.longitude,}} title={"Your current location"} />
              <Marker coordinate={{latitude: this.state.destination.lat,
              longitude: this.state.destination.lng,}} title={this.state.item.name} />
                <MapViewDirections
                    origin={this.state.origin}
                    destination={{latitude: this.state.destination.lat,
                      longitude: this.state.destination.lng,}}
                    apikey={"AIzaSyDipD4YgaXM7DZSEKzY_6E_Ekc08jUJ1n4"}
                    strokeWidth={5}
                    strokeColor="black"
                />
            </MapView> :
            <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}><ActivityIndicator size="large" color={COLORS.PEACH} /></View>
        } 
        
      </View>
    );
    
  }
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00000000'
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  header: {
    flexDirection: "row",
    paddingTop: 30,
    paddingBottom: 20,
    paddingHorizontal: 20,
    //justifyContent: 'center',
    alignItems: 'center',
  },
  topSection: {
    backgroundColor: COLORS.YELLOWVAR,
    borderBottomColor: COLORS.PEACH,
    borderBottomWidth: 5
  }, 
  title: {
    fontSize: width * 0.07,
    fontWeight: 'bold',
    paddingHorizontal: 10,
    color: COLORS.BLACK
  },
});