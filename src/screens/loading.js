import React from 'react';
import {View, StyleSheet, ActivityIndicator, Image} from 'react-native'
import { COLORS } from '../utils/constants'


export default function UpdatePrices(props){

    return (
        <View style={styles.container}>
            <View>
                <ActivityIndicator size="large" color={COLORS.PEACH} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#f4cd80"
    }
})