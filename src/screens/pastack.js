import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView, TextInput, Alert, Image} from 'react-native'
import { COLORS, PAYSTACK } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-community/async-storage';
import {CREATE_ORDER} from '../utils/functions'
import PaystackWebView from 'react-native-paystack-popup';


export default function Paystack(props){
    //const { name } = props.route.params;
    const [itemObj, setItem] = useState(null)
    const [dataObj, setData] = useState(null)


    useEffect(()=>{
        let isSubscribed = true

        if(isSubscribed){
            const item = JSON.parse(props.route.params.item);
            setItem(item)
            console.log()
        }

        return () => {
            isSubscribed = false
        }
    }, [])


    useEffect(()=>{
       
        const item = JSON.parse(props.route.params.item);
        const data = JSON.parse(props.route.params.data)
        setItem(item)
        setData(data)
        console.log(item)
        
    },[])
  
    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.goBack()}><IonIcons color={COLORS.BLACK} size={45} name="ios-arrow-round-back" /></TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>Payment</Text>
                    </View>
                </View>
            </View>
            <PaystackWebView

                ref={ref} 
                
                onError={() => {

                    setShowPayment(false);

                    Alert.alert("Transaction cancelled")

                }}
                currency="GHS"
                metadata={{ custom_fields: [{
                    display_name: `Payment of ${itemObj && itemObj.parent_brand.currency_symbol} ${amount} for ${itemObj && itemObj.fuelName} at ${itemObj.name}`
                }] }}

                onDismissed={() => {
                    setShowPayment(false);
                    //ref.current.reload(); //reload if dismissed.
                    alert(`Transaction cancelled`) 

                }}
                billingName
                onSuccess={(response) => { 
                
                    setShowPayment(false);
                    alert(`Transaction successful: ${response.reference}`) 
                    MakePayment()
                
                }}
                customerFirstName={dataObj && dataObj.firstname}
                customerLastName={dataObj && dataObj.lastname}
                indicatorColor={COLORS.PEACH}
                paystackKey={PAYSTACK.PUBLIC_KEY} 
                customerEmail={dataObj && dataObj.email} 
                amount={dataObj && parseFloat(dataObj.amount) * 100} />
        </View>
    )
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        flexDirection: "row",
        paddingTop: 30,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 20,
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
    },
    inputContainer: {
        paddingVertical: 30,       
    },
    platform: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        //paddingVertical: 10,   
    },
    input:{
        marginTop: 10,
        borderBottomWidth: 1,
        color: COLORS.BLACK
    },
    inputTitle:{
        fontWeight: 'bold'
    },
    saveBtn: {
        width: '100%',
        height: 40,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        borderRadius:5
    },
    btnText: {
        color: 'black',
        fontSize: 15,
    },
})