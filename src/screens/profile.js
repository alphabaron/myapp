import React, { useEffect, useState } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, TextInput, ScrollView} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import ScrollableTabView, {DefaultTabBar} from 'react-native-scrollable-tab-view'
import UpdatePrices from './update-prices'
import AsyncStorage from '@react-native-community/async-storage';
import { UPDATE_USER, STOREDATA } from '../utils/functions';

export default function Profile(props){

    //const userData = JSON.parse(props.route.params.userData);
    const [userData, setData] = useState(null)
    const [userId, setuserId] = useState("")
    const [firstname, setFname] = useState("")
    const [lastname, setLname] = useState("")
    const [phone, setPhone] = useState("")
    const [email, setEmail] = useState("")
    const [wallet, setWallet] = useState("")
    const [isComplete, setComplete] = useState('false')
    const [btnText, setBtnText] = useState("Save")
    const [isDisabled, setDisabled] = useState(false)

    function checkData(data){
        setuserId(data._id)
        setFname(data.firstname || "")
        setLname(data.lastname || "")
        setPhone(data.phone)
        setEmail(data.email || "")
    }

    async function save(){
        setDisabled(true)
        setBtnText("Saving...")
        if(firstname && lastname){
            var data = {
                firstname: firstname,
                lastname: lastname,
                userId: userId
            }

            try {
                var c = await UPDATE_USER(data)
                if(c.success){
                    console.log(c.data)
                    //store user data
                    STOREDATA('@userData', JSON.stringify(c.data))
                    checkData(c.data)
                    setDisabled(false)
                    setBtnText("Save")
                    props.navigation.navigate("Home", {userData: c.data})
                } else {
                    console.log(c.message)
                    setDisabled(false)
                    setBtnText("Save")
                }
            } catch (error) {
                console.log(error)
                setDisabled(false)
                setBtnText("Save")
            }
            
        } else {
            console.log('some fields are missing')
            setDisabled(false)
            setBtnText("Save")
        }
    }

    useEffect(()=>{
        let isSubscribed = true

        if(isSubscribed){
            const getData = async () => {
                try {
                  const values = await AsyncStorage.getItem('@userData')
                  if(values) {
                        
                    console.log(values[0][1])
                    console.log(values[1][1])
                    //console.log(JSON.parse(value))
                        setData(JSON.parse(values[0][1]))
                        let a = JSON.parse(values[0][1])
                        console.log(a)
                        checkData(a)
    
                        setComplete(values[1][1])
                        let b = values[0][1]
                        console.log(b)
                        
                  }
                } catch(e) {
                  // error reading value
                  console.log(e)
                }
            }
    
            getData()
        }

        return () => {
            isSubscribed = false
        }
        
        //console.log(props)
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    {isComplete === 'true' && <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.toggleDrawer()}><IonIcons color={COLORS.NAVYBLUE} size={45} name="ios-menu" /></TouchableOpacity>
                    </View>}
                    <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>Set up your profile</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tabbar}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.inputContainer}>
                    <Text>Firstname</Text>
                    <TextInput placeholder="Firstname" value={firstname} onChangeText={text => setFname(text)} style={{...styles.input, borderBottomColor: COLORS.BLACK}} />
                </View>
                <View style={styles.inputContainer}>
                    <Text>Lastname</Text>
                    <TextInput placeholder="Lastname" value={lastname} onChangeText={text => setLname(text)} style={{...styles.input, borderBottomColor: COLORS.BLACK}} />
                </View>
                
                <View style={styles.inputContainer}>
                    <Text>Phone number</Text>
                    <TextInput placeholder="Phone number" editable={false} value={phone} style={{...styles.input, color: COLORS.ASH, borderBottomColor: COLORS.BLACK}} />
                </View>
                
                <TouchableOpacity disabled={isDisabled} style={styles.saveBtn} onPress={save}>
                    <Text style={styles.btnText}>{btnText}</Text>
                </TouchableOpacity>
            </ScrollView>
            </View>
        </View>
    )
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        flexDirection: "row",
        paddingTop: 50,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 20,
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    inputContainer: {
        paddingBottom: 30
    },
    input:{
        fontSize: 20,
        marginTop: 5,
        borderBottomWidth: 1,
        color: COLORS.BLACK
    },
    saveBtn: {
        width: '100%',
        height: 40,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        borderRadius:5
    },
    btnText: {
        color: 'black',
        fontSize: 15,
    },
})