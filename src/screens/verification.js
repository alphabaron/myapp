import React, {useState} from 'react'; 
import {View, Text, StyleSheet, Dimensions, TouchableOpacity} from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import {COLORS, CONFIG} from '../utils/constants'
import { VERIFY_USER, httpRequest } from '../utils/functions';
import { useNavigation } from '@react-navigation/native';
import AuthContext from '../context'
import AsyncStorage from '@react-native-community/async-storage';

export default function Verification(props){
    const { signIn } = React.useContext(AuthContext);

    const navigation = useNavigation();

    const [btnText, setBtnText] = useState("Verify")
    const [isDisabled, setDisabled] = useState(false)
    const [message, setMessage] = useState("Enter verification code")
    const [otpCode, setOtpCode] = useState("")

    const { userId } = props.route.params;
    //const  userObj  = JSON.parse(props.route.params.userObj);

    async function verify(){
        setBtnText("Verifying...")

        var data = {
            otpCode: otpCode
        }
        try {
            const URL = `${CONFIG.BASE_URL}/api/v1/users/verifyuser/${userId}`
            var c = await httpRequest(URL, "put", data);
            console.log(c.response)
            if(c && c.code === 200){
                console.log("THere")
                console.log(c.response)
                setDisabled(false)
                setBtnText("Verify")
                //setInfo(c.message)

                var auth = {
                    auth: true,
                    userId: userId
                }
               
                signIn(auth)

            }else {
                console.log("Here")
                console.log(c.message)
                setDisabled(false)
                setBtnText("Verify")
                setInfo(c.message)
                throw c.message
            }
        } catch (error) {
            setDisabled(false)
            console.log(error)
        }
    }

    return (
        <View style={styles.container}>
            <View style={{paddingHorizontal: 20, paddingVertical: width * 0.1}}>
                <Text style={{fontSize: 30, fontWeight: 'bold', textAlign: 'center'}}>Verify</Text>
            </View>
            <View style={styles.tabbar}>
                    <Text style={{textAlign: 'center'}}>{message}</Text>
                    <OTPInputView
                        style={{flexDirection: 'row', justifyContent: 'center',alignItems: 'center', paddingVertical: 30, paddingHorizontal: 30}}
                        pinCount={4}
                        // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        // onCodeChanged = {code => { this.setState({code})}}
                        autoFocusOnLoad
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={styles.underlineStyleHighLighted}
                        onCodeFilled = {(code => {
                            setOtpCode(code)
                            console.log(`Code is ${code}, you are good to go!`)
                        })}
                        
                    />
                  
                    <TouchableOpacity disabled={isDisabled} style={styles.login} onPress={() => {verify()}}>
                        <Text style={styles.textLogin}>{btnText}</Text>
                    </TouchableOpacity>
            </View>
        </View>
    )
}


const width = Dimensions.get("screen").width
const height = Dimensions.get("screen").height
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        paddingTop: width * 0.5
    },
    tabbar: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        paddingHorizontal: 20,
        paddingVertical: 20
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },
    
    borderStyleHighLighted: {
        borderColor: COLORS.YELLOW,
    },
    
    underlineStyleBase: {
        width: width * 0.17,
        height: width * 0.17,
        borderWidth: 1,
        borderRadius: 20,
        borderColor: COLORS.YELLOW,
        backgroundColor: '#F1B64F4C',
        justifyContent: 'center',
        alignItems: 'center',
        color: COLORS.BLACK,
        fontSize: 20,
        fontWeight: 'bold'
    },
    
    underlineStyleHighLighted: {
        borderColor: COLORS.BLACK,
        color: COLORS.BLACK,
        fontSize: 20
    },
    login: {
        width: '100%',
        height: 40,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        borderRadius:5,
    },
})