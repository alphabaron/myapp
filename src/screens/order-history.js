import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, FlatList, RefreshControl, ActivityIndicator} from 'react-native'
import { COLORS } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import ScrollableTabView, {DefaultTabBar} from 'react-native-scrollable-tab-view'
import UpdatePrices from './update-prices'
import Notifications from './notifications'
import Branches from './branches'
import BrandProfile from './brand-profile'
import { GET_ORDERS } from '../utils/functions';
import AsyncStorage from '@react-native-community/async-storage';
import AuthContext from '../context';

export default function OrderHistory(props){

    const { signOut } = useContext(AuthContext);

    const [userId, setUserId]  = useState("")
    //const { name } = props.route.params;
    const [refreshing, setRefreshing] = useState(false);
    const [history, setHistory] = useState(null)
    function wait(timeout) {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getUserId()
        wait(2000).then(() => setRefreshing(false));
    }, [refreshing]);

    const ItemComponent = ({item}) => (
        <View style={styles.item}>
            <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
                <Text style={styles.subs}>Order No: </Text>
                <Text style={{justifyContent: 'flex-end'}}> #{item.order_code}</Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.subs}>Amount: </Text>
                <Text style={{textAlign: 'right', alignItems: 'flex-start'}}> GH₵ {item.amount}</Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.subs}>Fuel Station: </Text>
                <Text style={{textAlign: 'right'}}>{item.branch.name} </Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.subs}>Date: </Text>
                <Text style={{textAlign: 'right'}}>{new Date(item.created_at).toDateString()}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.subs}>Status: </Text>
                <Text style={{textAlign: 'right', color: item.paid ? "green" : "red"}}>{item.paid ? "Paid" : "Unpaid"}</Text>
            </View>
            
        </View>

    )

    async function getAllOrderHistory(userId){
        var c = await GET_ORDERS(userId);
        if(c.success){
            let a = c.data.reverse()
            //a = a.slice(0, 5)
            setHistory(a)

        } else {
            console.log(c.messsage)
        }
    }


    async function getUserId(){
        try {
            var value = await AsyncStorage.getItem("userId")
           // console.log("Help",value)
            if(value !== null){
                setUserId(value)
                await getAllOrderHistory(value)
            } else {
                console.log("failed")
                signOut()
            }
        } catch (error) {
            console.log(error)
            signOut()
        }
    }

    useEffect(()=>{
        getUserId()
    }, [])

   
    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent: 'flex-start', justifyContent: 'flex-start'}}>
                        <TouchableOpacity onPress={()=> props.navigation.toggleDrawer()}><IonIcons color={COLORS.BLACK} size={45} name="ios-menu" /></TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center',justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>History</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tabbar}>
            {history ? history.length > 0 ? 
                <FlatList
                    data={history}
                    renderItem={ItemComponent}
                    keyExtractor={(item, index)=> index.toString()}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }
                />
            :  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <Text> No orders made yet</Text>
                </View> 
            :   <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                    <ActivityIndicator size="large" color={COLORS.PEACH} />
                </View>
            
            }
            </View>
        </View>
    )
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    subs: {
        fontWeight: 'bold',
        justifyContent: 'flex-end'
    },
    header: {
        flexDirection: "row",
        paddingTop: 50,
        paddingBottom: 20,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
        borderBottomColor: COLORS.PEACH,
        borderBottomWidth: 5
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center',
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        paddingVertical: 20,
        backgroundColor: "#E5E5E5",
        paddingVertical: 20,
        paddingHorizontal: 20,
        borderRadius: 10,
        marginVertical: 5
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
})