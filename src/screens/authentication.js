import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView, TextInput, KeyboardAvoidingView} from 'react-native'
import { COLORS, CONFIG } from '../utils/constants';
import IonIcons from 'react-native-vector-icons/Ionicons'
import ScrollableTabView, {DefaultTabBar} from 'react-native-scrollable-tab-view'
import { LOGIN, REGISTER, httpRequest } from '../utils/functions';
import AsyncStorage from '@react-native-community/async-storage';
import IntlPhoneInput from 'react-native-intl-phone-input';
import {data} from '../countries'
import {Picker} from '@react-native-community/picker';
import * as Device from 'expo-device';


export default function Authentication(props){

    
    const Register = () => {
        const [firstname, setFname] = useState("")
        const [lastname, setLname] = useState("")
        const [phone, setPhone] = useState("")
        const [network, setNetwork] = useState("")
        const [isDisabled, setDisabled] = useState(false)
        const [btnText, setBtnText] = useState("Sign Up")
        const [focusVal, setFocusVal] = useState("")
        const [info, setInfo] = useState("")
        const [country, setCountry] = useState("")
        const [countryCode, setCountryCode] = useState("")
        const [dialCode, setDialCode] = useState("")

        const onChangeText = ({dialCode, unmaskedPhoneNumber, phoneNumber, isVerified}) => {
            //console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
            setPhone(dialCode + unmaskedPhoneNumber);
            var country = data.filter(c => c.dial_code == dialCode)[0]
            setCountry(country.name)
            setCountryCode(country.code)
            setDialCode(dialCode)
            //console.log(country)
        };
       // const {item} = props.route.params

       
    
        async function createAccount(){
            //console.log(phone)
            if(phone.length > 10){
                setBtnText("Please wait...")
                setDisabled(true)
                
                let data = {
                    firstname: firstname,
                    lastname: lastname,
                    phone: phone,
                    country: country,
                    countryCode: countryCode,
                    dialCode: dialCode,
                    deviceInfo: {
                        "brand": Device.brand || null,
                        "manufacturer": Device.manufacturer || null,
                        "model": Device.modelName || null,
                        "osVersion": Device.osVersion || null,
                    },
                    network: network
                }

                try {
                    const URL = `${CONFIG.BASE_URL}/api/v1/users/createuser`
                    var c = await httpRequest(URL, "post", data);
                    console.log(c)
                    if(c && c.code === 200){
                        setBtnText("Register")
                        console.log(c.response)
                        setDisabled(false)
                        props.navigation.navigate('Verification', {userId: c.response._id})

                    }else {
                        setBtnText("Register")
                        console.log("Here")
                        console.log(c.message)
                        setDisabled(false)
                        setInfo(c.message)
                        throw c.message
                    }
                } catch (error) {
                    setDisabled(false)
                    console.log(error)
                }
            }
            else {
                setDisabled(false)
                setInfo("Phone number invalid")
            }
            
            
        }

        return(
            <View showsVerticalScrollIndicator={false} style={{paddingHorizontal: 20, paddingVertical: 10}}>
                <View style={styles.action}>
                    <KeyboardAvoidingView>
                    <View style={{...styles.inputSection, borderColor: 'gray'}}>
                        <TextInput placeholder="First name" value={firstname} onFocus={()=>{setFocusVal("fname");}} onChangeText={(text)=>{setFname(text)}} style={{fontSize: 18, color: 'black'}} />
                    </View>
                    <View style={{...styles.inputSection, borderColor: 'gray'}}>
                        <TextInput placeholder="Last name" value={lastname} onFocus={()=>{setFocusVal("lname");}} onChangeText={(text)=>{setLname(text)}} style={{fontSize: 18, color: 'black'}} />
                    </View>
                    <View style={{...styles.phoneInputSection, borderColor: 'gray'}}>
                        <IntlPhoneInput onChangeText={onChangeText} onPress={()=>{setFocusVal("phone");}} defaultCountry="GH" phoneInputStyle={{fontSize: 18, color: 'black'}} dialCodeTextStyle={{fontSize: 18, color: 'black'}} flagStyle={{marginRight:5}} containerStyle={{paddingVertical: 0, paddingHorizontal: 0, border: 0}} placeholder={" (Enter phone) "}  />
                        <Text>Phone number will be used for in app payments</Text>
                    </View>
                    <View style={{...styles.phoneInputSection, borderColor: 'gray'}}>
                        <Text>Choose Network</Text>
                        <Picker
                            selectedValue={network}
                            style={{...styles.inputSection}}
                            onValueChange={(itemValue, itemIndex) => setNetwork(itemValue)}
                        >
                            <Picker.Item label="MTN" value="MTN" />
                            <Picker.Item label="AIRTEL TIGO" value="TIGO" />
                            <Picker.Item label="VODAFONE" value="VODAFONE" />
                        </Picker>
                    </View>
                    </KeyboardAvoidingView>
                </View>
                <TouchableOpacity disabled={isDisabled} style={styles.login} onPress={createAccount}>
                    <Text style={styles.textLogin}>{btnText}</Text>
                </TouchableOpacity>             
                <Text style={{textAlign: 'center', paddingVertical: 10}}>{info}</Text>  
            </View>
        )
    }



    const Login = () => {

        const [phone, setPhone] = useState("")
        const [isDisabled, setDisabled] = useState(false)
        const [btnText, setBtnText] = useState("Sign In")
        const [focusVal, setFocusVal] = useState("")
        const [info, setInfo] = useState("")

        const onChangeText = ({dialCode, unmaskedPhoneNumber, phoneNumber, isVerified}) => {
            console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
            setPhone(dialCode + unmaskedPhoneNumber);
            
        };

        async function loginReq(){

            if(phone.length > 10){
                setBtnText("Please wait...")
                setDisabled(true)

                let data = {
                    phone: phone
                }

                try {
                    const URL = `${CONFIG.BASE_URL}/api/v1/users/login`;
                    var c = await httpRequest(URL, "post", data);
                    console.log(c.response)
                    if(c && c.code === 200){
                        //console.log("THere")
                        console.log(c.response)
                        setDisabled(false)
                        setBtnText("Login")
                        //setInfo(c.message)
                       
                        props.navigation.navigate('Verification', {userId: c.response._id, userObj: JSON.stringify(c.response)})
        
                    }else {
                        console.log(c.message)
                        setDisabled(false)
                        setBtnText("Login")
                        setInfo(c.message)
                        throw c.message
                    }
                } catch (error) {
                    setDisabled(false)
                    console.log(error)
                }
            }

        }

        return(
            <ScrollView showsVerticalScrollIndicator={false} style={{paddingHorizontal: 20, paddingVertical: 20}}>
                <View style={styles.action}>
                   
                    <View style={{...styles.phoneInputSection, borderColor: focusVal==="phone" ? COLORS.PEACH : 'gray'}}>
                        {/*<TextInput placeholder="Phone number" value={phone} onFocus={()=>{setFocusVal("phone");}} onChangeText={(text)=>{setPhone(text)}} maxLength={10}  keyboardType="number-pad" style={{fontSize: 18, color: focusVal==="phone" ? COLORS.PEACH : 'gray'}} />*/}
                        <IntlPhoneInput onChangeText={onChangeText} onFocus={()=>{setFocusVal("phone");}} defaultCountry="GH" phoneInputStyle={{fontSize: 18, color: focusVal==="phone" ? COLORS.PEACH : 'gray'}} dialCodeTextStyle={{fontSize: 18, color: focusVal==="phone" ? COLORS.PEACH : 'gray'}} flagStyle={{marginRight:5}} containerStyle={{paddingVertical: 0, paddingHorizontal: 0, border: 0}} placeholder={" (Enter phone) "}  />
                    </View>
                </View>
                <TouchableOpacity disabled={isDisabled} style={styles.login} onPress={loginReq}>
                    <Text style={styles.textLogin}>{btnText}</Text>
                </TouchableOpacity>  
                <Text style={{textAlign: 'center', paddingVertical: 10}}>{info}</Text>        
            </ScrollView>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.topSection}>
                <View style={styles.header}>
                    <View style={{alignContent:'center', justifyContent:'center', paddingHorizontal: 10}}>
                        <Text style={styles.title}>Welcome</Text>
                    </View>
                </View>
            </View>

            <View style={styles.tabbar}>
                    <ScrollableTabView
                        initialPage={0}
                        tabBarActiveTextColor={COLORS.PEACH}
                        tabBarTextStyle={{fontSize: 18}}
                        tabBarBackgroundColor={COLORS.YELLOWVAR}
                        renderTabBar={()=> <DefaultTabBar underlineStyle={{backgroundColor: COLORS.PEACH}}/>}
                    >
                        <Login tabLabel="Sign In" {...props}/>
                        <Register tabLabel="Sign Up" {...props}/>
                    </ScrollableTabView>
            </View>
        </View>
    )
}

const width = Dimensions.get("screen").width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.WHITE
    },
    header: {
        paddingTop: width * 0.2,
        paddingBottom: width * 0.1,
        paddingHorizontal: 20,
        //justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: width * 0.07,
        fontWeight: 'bold',
        paddingHorizontal: 10,
        color: COLORS.BLACK,
        textAlign: 'center'
    },
    topSection: {
        backgroundColor: COLORS.YELLOWVAR,
    },
    inputSection: {
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 15,
        paddingVertical: 10,
        justifyContent: 'center',
        marginTop: 10
    },
    phoneInputSection: {
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        paddingVertical: 3,
        justifyContent: 'center',
        marginTop: 10
    },
    topButtons: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    topItem: {
        flex: 1,
        flexDirection: 'row',
        fontWeight: 'bold',
        justifyContent: 'center',
        alignContent: 'center'
    },
    icon: {
        marginLeft: 10,
    },
    topText: {
        fontSize: 16
    },
    content: {
        flex: 1,
        paddingHorizontal: 10,
        paddingVertical: 20
    },
    item: {
        flexDirection: 'row',
        paddingVertical: 20,
    },
    avatar: {
        padding: 5
    },
    petrol: {
        backgroundColor: COLORS.PETROL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    diesel: {
        backgroundColor: COLORS.DIESEL,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    vpower: {
        backgroundColor: COLORS.VPOWER,
        padding: 15,
        borderRadius: 20,
        shadowColor: '#0000001A',
        shadowOffset: { width: 1, height: 3 },
        shadowOpacity: 0.8,
        shadowRadius: 20,
        marginHorizontal: 10
    },
    fuelname: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        alignContent: 'center'
    },
    fuelprice:{
        fontSize: 12,
        marginTop: 10,
    },
    tabbar: {
        flex: 1,
    },
    inputContainer: {
        paddingVertical: 30,       
    },
    input:{
        fontSize: 20,
        marginTop: 10,
        borderBottomWidth: 1,
        color: COLORS.BLACK
    },
    saveBtn: {
        width: '100%',
        height: 40,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        borderRadius:5
    },
    btnText: {
        color: 'black',
        fontSize: 15,
    },
    textLogin: {
        color: COLORS.BLACK,
        fontSize: 17,
    },
    login: {
        width: '100%',
        height: 40,
        backgroundColor: COLORS.YELLOW,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
        borderRadius:5
    },
})